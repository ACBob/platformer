"""
Platformer, As of yet un-named.
    Copyright (C) 2020  ACBob

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import colour
import toml

colours_file = toml.loads(open('colours.toml','r').read())
colours = {}
for colour_line in colours_file:
    colours[colour_line] = tuple(255*x for x in colour.Color(colours_file[colour_line]).rgb)

def get_xkcd_name(name):
    return colours.get(name, colours['shit'])
