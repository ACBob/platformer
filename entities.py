"""
Platformer, As of yet un-named.
    Copyright (C) 2020  ACBob

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import pygame
import pygame.locals
import json
import enum
import constants
import toml

import material

class block_type(enum.IntEnum):
    """IntEnum of block_types.
        TL_MAX is intended to be used for checking the highest enum value, and is always the last block_type+1.
        Garunteed Values:
         TL_NULL -> 0
         TL_MAX -> size of enum values"""
    TL_NULL = 0
    TL_GRND = 1
    TL_ICE = 2
    
    TL_MAX = 3#NOTE : stay last

    @classmethod
    def _missing_(cls, value):
        print("{} is not a valid block_type.".format(value))
        return block_type.TL_NULL

vector = pygame.math.Vector2

class sprite_base:
    """A generic sprite base class, usually extended by other sprites."""
    def __init__(self,pos,tex,mass=1):
        """Init the sprite class.
            pos -> position of sprite, in-world
            tex -> texutre for sprite to use, uses internal material system (NoneType also works.)
            mass=1 -> the mass of the sprite."""

        self.pos = vector(pos)
        self.velocity = vector([0.0,0.0])
        self.acceleration = vector([0.0,0.0])
        self.gravity = constants.GRAVITY

        try:
            if not tex:
                raise FileNotFoundError("Nonetype")
            self.material = material.load_material(tex)
            if type(self.material) == dict:
                self.texture = self.material['tex']
            else:
                self.texture = self.material
                self.material = {}
        except (FileNotFoundError,pygame.error):
            self.texture = pygame.image.load('material/error.png') #TODO: material system
            self.material = {'basetex':'material/error.png','tex':self.texture}
        
        self.collision = pygame.rect.Rect(pos,self.texture.get_size())

    def test_collision(self,colliders):
        """Returns wether or not this sprite has collided with any rects in the list.
            colliders -> a list of rect-style objects, to test against."""
        return (self.get_collision(colliders) > -1)

    def get_collision(self,colliders):
        """Returns the index of rect that collides with this sprite's collision.
            colliders -> a list of rect-style objects, to test against."""
        return self.collision.collidelist(colliders)

    def get_attributes(self):
        """Returns some general attributes of this sprite, as a tuple.
            return parameters:
                [0] = position (pygame vector)
                [1] = texture (pygame surface)
                [2] = collision (pygame rect)"""
        return ( self.pos, self.texture, self.collision)

    def update(self,colliders): #STUB
        """Stub. Classes should extend this function.
            colliders -> a list of rect-style objects, for this function to operate on."""
        return
        
    def render(self,display,offset,screen_size):
        """Blits this sprite's surface on the designated surface.
            display -> pygame surface to blit onto.
            offset -> a vector denoting the "camera". (0,0) would render it with no offset.
            screen_size -> the size of the pygame display screen."""
        size = self.collision.size
        display.blit(self.texture, (self.pos.x-offset.x+(screen_size[0]/2)-size[0]/2,self.pos.y-offset.y+(screen_size[1]/2)-size[1]/2))

    def render_editor(self,display,offset,screensize):
        """A special render mode, designed for use in the editor."""
        display.blit(self.texture, (self.pos.x-offset.x,self.pos.y-offset.y))

class sprite_tile(sprite_base):
    """An extension of the sprite_base class, that adds attributes and functions relating to being an in-world object."""
    def __init__(self,block_id,pos,this_block_type,args,colliders=[]):
        """Init the sprite class.
            block_id -> Internal block ID, intended for scripts.
            pos -> Position of this tile (from the top-left corner.)
            this_block_type -> a value from the enum block_type, specifying what this block is. Sets "DEFAULT" texture and other attributes.
            args -> general dictionary of arguments to pass to this."""
        if args['TEX'] == "DEFAULT": #DEFAULT is a keyword that states choose the default for blocktype and situation.
            tex = block_properties[this_block_type.name]['defaulttex']
            self.tex_name = "DEFAULT"
        elif args['TEX'] == "NONE":
            self.tex_name = "NONE"
            tex = None
        else:
            tex = args['TEX']
            self.tex_name = tex
        super().__init__(pos,tex) #Init the SpriteBase class

        self.ID = block_id #For scripts
        self.block_type = this_block_type #Mostly for exapnsion. no idea what i'm gonna do with it for now.

        self.friction = block_properties[this_block_type.name]['friction']
        self.edit_glow = block_properties[this_block_type.name]['edit_glow']

        self.figure_out_connections(colliders)

    def figure_out_connections(self,colliders):
        if 'joins' in self.material:
            self.connections = []

            size = self.material['joins']['single'].get_size()
            self.collision = pygame.rect.Rect(self.pos,size)
            #R
            self.connections.append( (self.collision.move((size[0],0)).collidelist(colliders)) > -1)
            #U
            self.connections.append( (self.collision.move((0,-size[1])).collidelist(colliders)) > -1)
            #L
            self.connections.append( (self.collision.move((-size[0],0)).collidelist(colliders)) > -1)
            #D
            self.connections.append( (self.collision.move((0,size[1])).collidelist(colliders)) > -1)

            #Set our material accordingly
            if not self.connections[0] and not self.connections[1] and not self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['single']

            elif self.connections[0] and self.connections[1] and self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['allfour']

            elif not self.connections[0] and self.connections[1] and not self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['updown']
            elif self.connections[0] and not self.connections[1] and self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['leftright']

            elif self.connections[0] and not self.connections[1] and not self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['downright']
            elif not self.connections[0] and not self.connections[1] and self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['downleft']
            elif self.connections[0] and self.connections[1] and not self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['upright']
            elif not self.connections[0] and self.connections[1] and self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['upleft']
                
            elif self.connections[0] and not self.connections[1] and not self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['right']
            elif not self.connections[0] and not self.connections[1] and self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['left']
            elif not self.connections[0] and self.connections[1] and not self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['top']
            elif not self.connections[0] and not self.connections[1] and not self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['bottom']

            elif self.connections[0] and self.connections[1] and not self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['tleft']
            elif not self.connections[0] and self.connections[1] and self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['tright']
            elif self.connections[0] and self.connections[1] and self.connections[2] and not self.connections[3]:
                self.texture = self.material['joins']['tup']
            elif self.connections[0] and not self.connections[1] and self.connections[2] and self.connections[3]:
                self.texture = self.material['joins']['tdown']

            else:
                self.texture = self.material['joins']['single']

    def dumps(self):
        """Dump the attributes of the tile as a string."""
        return ','.join([str(self.ID),self.block_type.name,str(round(self.pos[0])),str(round(self.pos[1])),"{\"TEX\":\""+self.tex_name+"\"}"])

    def dump(self):
        """Dump the attributes of the tile as a list.
            [0] -> Block's ID
            [1] -> Block's type
            [2] -> Position vector
            [3] -> DICT Arguments"""
        return [self.ID,self.block_type,self.pos,{"TEX":self.tex_name}]

    def get(self,attr=None):
        """Get the specific attribute by attr.
            if attr is an invalid value (i.e not in the following strings), or
            is NoneType, it will return the output of .dump().
            \"id\" -> ID
            \"block_type_name\" -> string name for enum value of block type
            \"block_type_value\" -> enum value for block type
            \"pos\" -> Position vector
            \"tex_name\" -> Texture name"""
        if attr == "id":
            out = self.ID
        elif attr == "block_type_name":
            out = self.block_type.name
        elif attr == "block_type_value":
            out = self.block_type.value
        elif attr == "pos":
            out = self.pos
        elif attr == "tex_name":
            out = self.tex_name
        else:
            return self.dump()
        return out

    def render_editor(self,display,offset,screensize):
        """A special render mode, designed for use in the editor."""
        super().render_editor(display,offset,screensize)
        if self.edit_glow:
            glow = self.collision.copy()
            glow.topleft = (self.pos.x-offset.x,self.pos.y-offset.y)
            pygame.draw.rect(display, (0,255,0), glow, 2)

class sprite_tile_error(sprite_tile):
    """An extension of the sprite_tile class, used for block_type.TL_NULL entries."""
    def __init__(self,block_id,pos):
        """Init the error class.
            block_id -> Internal block ID, intended for scripts.
            pos -> Position of this tile (from the top-left corner.)"""
        super().__init__(block_id,pos,block_type.TL_NULL,{"TEX":"NONE"})

    def render(self,display,offset,screen_size):
        """Stub. this sprite does not render under normal circumstances."""
        return

    def render_editor(self,display,offset,screen_size):
        """A special render mode, designed for use in the editor."""
        rect = self.collision.copy()
        rect.topleft = (self.pos.x-offset.x,self.pos.y-offset.y)
        pygame.draw.rect(display, (255,0,0), rect, 5)

class sprite_player(sprite_base):
    """An extension of the sprite_base class, that adds attributes and functions relating to physics and input."""
    def __init__(self,pos):
        super().__init__(pos,"material/player.toml",5) #TODO: material system
        self.applied_friction = constants.AIRFRICTION
        self.sprinting = False

    def jump(self,colliders):
        """The player's jump event. sets the player's upward velocity to -11.
            colliders -> a list of rect-style objects, used for checking if the player can jump."""
        self.pos.y += 1
        self.collision.topleft = self.pos
        if self.test_collision(colliders):
            self.velocity.y = -11
            self.acceleration.y = -10
        self.pos.y -= 1

    def update(self,inp,colliders,render_list):
        """Updates the player, intended for use on each frame.
            inp -> the inputs for this given frame.
            colliders -> a list of rect-style objects to test collision against.
            render_list -> a list of sprite_base-style objects."""
        self.acceleration = vector(0.0,self.gravity)

        self.sprinting = inp[pygame.K_LSHIFT]
        
        if inp[pygame.K_a]:
            if not self.sprinting:
                self.acceleration.x = 3
            else:
                self.acceleration.x = 6
        if inp[pygame.K_d]:
            if not self.sprinting:
                self.acceleration.x = -3
            else:
                self.acceleration.x = -6
        
        self.acceleration.x += self.velocity.x# * -self.SurfaceFriction
        self.acceleration.x *= -self.applied_friction
        self.velocity += self.acceleration

        #Move Along X
        self.pos.x += self.velocity.x + 0.5 * self.acceleration.x
        self.collision.topleft = self.pos

        #Test Collision        
        if self.test_collision(colliders):
            #Reset X
            if self.velocity.x > 0.0:
                self.pos.x = colliders[self.get_collision(colliders)].left-colliders[self.get_collision(colliders)].width
            elif self.velocity.x < 0.0:
                self.pos.x = colliders[self.get_collision(colliders)].right

            self.velocity.x = 0
            self.acceleration.x = 0

        #Move Along Y
        self.pos.y += 1 + self.velocity.y + 0.5 * self.acceleration.y
        self.collision.topleft = self.pos

        self.applied_friction = constants.AIRFRICTION

        #Test Collision
        if self.test_collision(colliders):
            #Reset Y
            self.applied_friction = constants.AIRFRICTION * render_list[self.get_collision(colliders)].friction
            if self.sprinting:
                self.applied_friction /= 2
            if self.velocity.y > 0.0:
                self.pos.y = colliders[self.get_collision(colliders)].top-colliders[self.get_collision(colliders)].height
            elif self.velocity.y < 0.0:
                self.pos.y = colliders[self.get_collision(colliders)].bottom

            self.velocity.y = 0
            self.acceleration.y = 0

        self.collision.topleft = self.pos #Update collision

        if self.velocity.dot(self.velocity) < 1e-3:
            self.velocity = vector(0,0)
        if self.acceleration.dot(self.acceleration) < 1e-3:
            self.acceleration = vector(0,0)

        #print(self.pos, self.velocity, self.acceleration)
    
def make_block(args,colliders=[]):
    """Generates and returns a sprite_tile entity based on the args.
        args -> string of "id,block_type,x,y,json_arguments\""""
    args = args.split(',')

    if not args[1] in block_type.__members__:
        return sprite_tile_error(0,(int(args[2]),int(args[3])))
    block_id = int(args[0])
    this_block_type = block_type[args[1]]
    block_pos = (int(args[2]),int(args[3]))
    block_tags = json.loads(args[4])

    return sprite_tile(block_id,block_pos,this_block_type,block_tags,colliders=colliders)

    

block_types_file = toml.load('block_types.toml')
block_properties = {'TL_NULL':{'defaulttex':None,'friction':0}}
for item in block_types_file:
    print(item)
    block_properties[item] = block_types_file[item]
block_type = enum.IntEnum("block_type",['TL_NULL']+list(block_types_file.keys())+['TL_MAX'])

if __name__ == "__main__":
    print("Testing Makeblock","0,TL_GRND,0,0,{\"TEX\":\"DEFAULT\"}")
    a = make_block("0,TL_GRND,0,0,{\"TEX\":\"DEFAULT\"}")
    print(a)
    print("Testing SpriteTile.dumps()")
    print(a.dumps())
