"""
Platformer, As of yet un-named.
    Copyright (C) 2020  ACBob

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import colour
import levelload
import entities
import toml
import pygame
import tkinter
import colours
from tkinter.filedialog import asksaveasfilename, askopenfilename

tk_root = tkinter.Tk()
tk_root.withdraw()

vector = pygame.math.Vector2

def render(display,render_list):
    for i in render_list:
        i.render_editor(display,camera,(800,416))

pygame.init()
pygame.display.init()

render_list = []

display = pygame.display.set_mode((800,416))

pygame.display.set_caption("Level Editor")

grid = pygame.Surface((800,416))
overlay = pygame.Surface((800,416))

overlay.set_alpha(64)

font = pygame.font.SysFont("Cousine",16)

grid.fill(colours.get_xkcd_name('white'))
#Draw Grid
for i in range(0,25):
    pygame.draw.line(grid, colours.get_xkcd_name("darkred"), (i*32,0),(i*32,416), 1)
for i in range(0,13):
    pygame.draw.line(grid, colours.get_xkcd_name("darkred"), (0,i*32),(800,i*32), 1)

game_running = True
camera = vector(0,0)

pygame.mouse.set_visible(False)

place_block_type = entities.block_type.TL_GRND
block_index = 0

while game_running:
    display.fill(colours.get_xkcd_name('babyshitgreen'))
    overlay.fill(colours.get_xkcd_name('black'))
    
    display.blit(grid, (0,0), special_flags=pygame.BLEND_MULT)

    mouse_pos = pygame.mouse.get_pos()
    mouse_rect = pygame.rect.Rect((round(mouse_pos[0]/32)*32,round(mouse_pos[1]/32)*32),(32,32))
    spawn_pos_x = round(round(mouse_pos[0]/32)*32+camera.x)
    spawn_pos_y = round(round(mouse_pos[1]/32)*32+camera.y)
    spawn_rect = pygame.rect.Rect((spawn_pos_x,spawn_pos_y),(32,32))

    collisions = []
    for i in render_list:
        collisions.append(i.collision)

    can_place = not (spawn_rect.collidelist(collisions) > -1)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_running = False
            break
        
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 4:
                block_index += 1
            elif event.button == 5:
                block_index -=1
            
            if block_index <= entities.block_type.TL_NULL:
                block_index = entities.block_type.TL_MAX-1
            elif block_index >= entities.block_type.TL_MAX:
                block_index = entities.block_type.TL_NULL+1

            place_block_type = entities.block_type(block_index)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_s:
                camera.y+=32
            elif event.key == pygame.K_w:
                camera.y-=32
            elif event.key == pygame.K_a:
                camera.x-=32
            elif event.key == pygame.K_d:
                camera.x+=32

            elif event.key == pygame.K_f:
                print("Saving...")
                result = asksaveasfilename(
                    filetypes=[("Level Files", "*.lvl")],
                    )
                if result:
                    level = {
                        'info':{
                            'title':'Change me',
                            'desc':'Change me'
                            },
                        'blocks':render_list,
                        'scripts':''
                        }
                    dump = levelload.dumps(level)

                    result = open(result,'wb')
                    result.write(dump)
                    result.close()
                    
            elif event.key == pygame.K_o:
                print("Opening...")
                result = askopenfilename(
                    filetypes=[("Level Files", "*.lvl")],
                    )
                if result:
                    render_list = levelload.load_level(result)['blocks']
        if pygame.mouse.get_pressed()[0]:
            if can_place:
                render_list.append(entities.make_block("0,"+place_block_type.name+","+str(spawn_pos_x)+","+str(spawn_pos_y)+",{\"TEX\":\"DEFAULT\"}"))
        elif pygame.mouse.get_pressed()[1]:
            clicked_block = spawn_rect.collidelist(collisions)
            if clicked_block > -1:
                block_index = render_list[spawn_rect.collidelist(collisions)].get("block_type_value")
                place_block_type = entities.block_type(block_index)
        elif pygame.mouse.get_pressed()[2]:
            clicked_block = spawn_rect.collidelist(collisions)
            if clicked_block > -1:
                try:
                    render_list.pop(clicked_block)
                except Exception as e:
                    print(e)
                    

    render(display,render_list)
    if can_place:
        pygame.draw.rect(overlay,colours.get_xkcd_name('white'),mouse_rect)
    else:
        pygame.draw.rect(display,colours.get_xkcd_name('bloodred'),mouse_rect,1)

    display.blit(overlay, (0,0))

    display.blit(font.render(place_block_type.name, False, (0,0,0)),mouse_pos)
    
    pygame.display.flip()

pygame.quit()
