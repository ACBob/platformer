import xml.etree.ElementTree as ET #The level files are *technically* SGML, but python only has XML.
import entities as ents
import re #We use regex to get the data from the xml tags.
import constants

magi_tag = re.compile('(?<=\[")(.*)(?="\])',flags=re.I|re.M)
magi_block_tag = re.compile('(?<=\[)(.*)(?=\])',flags=re.I|re.M)

class level_error(Exception):
    def __init__(self):
        super().__init__()

def load_level(path):
    level = ET.parse(path)
    level_root = level.getroot()

    level_dict = {}
    level_desc = {}
    level_blocks = []
    level_scripts = []

    #Not used
    colliders = []

    if int(level_root.attrib['format']) > constants.HIGHEST_MAP_VERSION:
        raise RuntimeError("Map Format ({map_f}) higher than what we support ({us_f})! Aborting...".format(map_f=level_root.attrib['format'], us_f=str(constants.HIGHEST_MAP_VERSION)))

    for child in level_root:
        if child.tag == "desc":
            if level_desc:
                raise level_error("Desc Redefined") #Only one <desc> please.
            desc = child.text.strip()
            desc = magi_tag.findall(desc)

            level_desc['title'] = desc[0] #Title, Desc. Hard-coded.
            level_desc['desc'] = desc[1]

            level_dict['info'] = level_desc
        elif child.tag == "blocks":
            if level_blocks:
                raise level_error("Blocks Redefined") #Only one <blocks> please.
            blocks = child.text.strip()
            blocks = magi_block_tag.findall(blocks)

            for block in blocks:
                block = ents.make_block(block,colliders)
                level_blocks.append(block)
                colliders.append(block.collision)

            level_dict['blocks'] = level_blocks

        elif child.tag == "script":
            level_scripts.append(child.text.strip()) #We allow multiple scripts, but they're ran in series.

    level_dict['scripts'] = level_scripts

    for i in level_blocks:
        i.figure_out_connections(colliders)

    if not level_desc or not level_blocks:
        raise level_error("Malformed Level")

    return level_dict

def dumps(level_dict):
    level = ET.Element('level')
    desc = ET.SubElement(level, 'desc')
    blocks = ET.SubElement(level, 'blocks')
    script = ET.SubElement(level, 'scripts')

    description = ["",""]
    block_strings = []

    for block in level_dict['blocks']:
        block_strings.append("[{}]".format(block.dumps()))

    if level_dict['info']:
        description[0] = "[\"{}\"]".format(level_dict['info']['title'])
        description[1] = "[\"{}\"]".format(level_dict['info']['desc'])
    else:
        description = ["Nameless Level","Un-named level."]

    desc.text = '\n'.join(description)
    blocks.text = '\n'.join(block_strings)

    level.attrib = {'format':str(constants.HIGHEST_MAP_VERSION)}

    return ET.tostring(level)

    
