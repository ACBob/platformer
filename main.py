"""
Platformer, As of yet un-named.
    Copyright (C) 2020  ACBob

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import pygame
import entities
import levelload
import toml
import constants
import colours

global render_list

def render_frame(window):
    """Renders the render_list"""
    for renderable in render_list:
        renderable.render(window,player.pos,screen_size)

def send_click(click):
    """Stub"""
    return

def send_middle_click(click):
    """Stub"""
    return

def send_right_click(click):
    """Stub"""
    return


game_running = True

pygame.init()
pygame.display.init()

screen_size = (800,416)
window = pygame.display.set_mode(screen_size)
player = entities.sprite_player((0,0))

render_list = [player]
game_level = levelload.load_level("levels/default.lvl")
render_list = render_list+game_level['blocks']
debug_font = pygame.font.Font("Fonts/BobishNew.ttf",16)
game_clock = pygame.time.Clock()

while game_running:
    window.fill(colours.get_xkcd_name('skyblue'))

    collisions = []
    for renderable in render_list[1:]:
        collisions.append(renderable.collision)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_running = False
            break
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1: send_click(event)
            elif event.button == 2: send_middle_click(event)
            elif event.button == 3: send_right_click(event)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                player.jump(collisions)

    player.update(pygame.key.get_pressed(),collisions,render_list[1:])

    render_frame(window)

    window.blit(debug_font.render(str(player.velocity)+" "+str(player.pos), True, (0,0,0)),(0,0))

    pygame.display.flip()
    game_clock.tick(60)

pygame.quit()
