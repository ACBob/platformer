"""
Platformer, As of yet un-named.
    Copyright (C) 2020  ACBob

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import pygame
import os
import toml
import colours

from PIL import Image

tex_cache = {}

tint_types = {
    "add": pygame.BLEND_RGBA_ADD,
    "mult": pygame.BLEND_RGBA_MULT,
    "max": pygame.BLEND_RGBA_MAX,
    "min": pygame.BLEND_RGBA_MIN,
    "sub": pygame.BLEND_RGBA_SUB
}

def tint_effect(surface, tint, alpha, tint_type):
    """The tint effect.
        Returns a tinted surface.
        surface -> Surface to tint
        tint -> Tint colour
        alpha -> Alpha of tint
        tint_type -> Blending type to use ("add","mult","max","min","sub") default: "mult"
    """
    tinted = surface.copy()
    tinter = surface.copy()
    tinter.set_alpha(alpha)
    
    if tint_types.get(tint_type, "default") == "default":
        tinter.fill((0,0,0,255), None, pygame.BLEND_RGBA_MULT)
    tinter.fill(tint[0:3] + (0,), None, tint_types.get(tint_type, pygame.BLEND_RGBA_ADD))

    tinted.blit(tinter, (0,0))
    return tinted

def load_material(path):
    """Load a given material path
        Returns dict, representing material.
        path -> path to material file.
    """
    if path.endswith('.png'):
        if not path in tex_cache.keys():
            print("Loading",path,"Should use Material System!")
            tex = pygame.image.load(path)
            tex_cache[path] = tex
        else:
            tex = tex_cache[path]
        return tex

    if not path in tex_cache.keys():
        print("Loading",path)
        material_file = toml.loads(open(path,'r').read())
        if not material_file['basetex'] in tex_cache.keys():
            material_file['tex'] = pygame.image.load(material_file['basetex'])
            tex_cache[material_file['basetex']] = material_file['tex']
        else:
            material_file['tex'] = tex_cache[material_file['basetex']]
        tex_cache[path] = material_file

        if 'hueshift' in material_file:
            raise NotImplementedError("HUESHIFT is currently not supported.")
        if 'tint' in material_file:
            tint_colour = (0,0,0)
            if type(material_file['tint']) == str:
                tint_colour = colours.get_xkcd_name(material_file['tint'])
            elif type(material_file['tint']) == list and len(material_file['tint']) == 3:
                tint_colour = material_file['tint']
            else:
                raise Exception("Invalid tint")

            alpha = 50
            if 'tintalpha' in material_file and type(material_file['tintalpha']) == int:
                alpha = material_file['tintalpha']
            
            tint_type = "default"
            if 'tinttype' in material_file and type(material_file['tinttype']) == str:
                tint_type = material_file['tinttype']
            material_file['tex'] = tint_effect(material_file['tex'], tint_colour, alpha, tint_type)

        if 'join' in material_file:
            for i in material_file['joins']:
                material_file['joins'][i] = material_file['tex'].subsurface((material_file['joins'][i][0], material_file['joins'][i][1]))
    else:
        material_file = tex_cache[path]

    return material_file

if __name__ == "__main__":
    load_material("material/jointest.toml")
