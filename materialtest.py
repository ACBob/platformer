"""
Platformer, As of yet un-named.
    Copyright (C) 2020  ACBob

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import colour
import levelload
import entities
import toml
import pygame
import tkinter
import colours
from tkinter.filedialog import askopenfilename

tk_root = tkinter.Tk()
tk_root.withdraw()

vector = pygame.math.Vector2

def render(display,render_list):
    for i in render_list:
        i.render_editor(display,camera,(800,416))

pygame.init()
pygame.display.init()

render_list = []

display = pygame.display.set_mode((800,416))

pygame.display.set_caption("Material Test")

grid = pygame.Surface((800,416))

font = pygame.font.SysFont("Cousine",16)

game_running = True

place_block_type = entities.block_type.TL_GRND
block_index = 0

clock = pygame.time.Clock()

class sprite_material_test(entities.sprite_tile):
    def __init__(self, material):
        super().__init__(0,(0,0),entities.block_type.TL_GRND, {'TEX':material})

        self.tex_index = 0

    def render(self, display):
        self.tex_index += 1
        texer = ''
        if 'joins' in self.material and self.tex_index > len(self.material['joins'])-1:
            self.tex_index = 0
        texer = list(self.material['joins'].keys())[self.tex_index]

        display.blit(self.material['joins'][texer], (394,292))
        display.blit(font.render(texer, True, (255,255,255)), (394,270))

while game_running:
    display.fill(colours.get_xkcd_name('babyshitbrown'))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_running = False
            break
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_o:
                print("Opening...")
                result = askopenfilename(
                    filetypes=[("Material Files", "*.toml")],
                    )
                if result:
                    render_list = []

                    tile = sprite_material_test(result)
    
                    render_list.append(tile)

    for i in render_list:
        i.render(display)
    
    pygame.display.flip()

    clock.tick(1) #Will feel sluggish as result

pygame.quit()
